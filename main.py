import matplotlib.pyplot as plt
import numpy as np

factor = 1.3247361138683294e-39
G = 6.67428e-11*factor
m1 = 1.024e26*factor
m2 = 8.681e25*factor
M = 1.989e30*factor

def qddx(qx, qy, acc):
    if(True):
        return -(acc*qx)/(qx*qx+qy*qy)**(3/2)
    return ((-G*M*m1)/abs(q1)**3)

def qddy(qx, qy,acc):
    if(True):
        return -(acc*qy)/(qx*qx+qy*qy)**(3/2)
    return ((-G*M*m1)/abs(q1)**3)




N = 10000
L = 50
h = (L/N)

qxd = np.zeros(N)
qyd = np.zeros(N)

qx = np.zeros(N)
qy = np.zeros(N)

qxj = np.zeros(N)
qyj = np.zeros(N)

qxm = np.zeros(N)
qym = np.zeros(N)


x = 1.0
xm = 1.5
xj = 5.2
y = 0
ym = 0
yj = 0
vx = 0
vy = 2*np.pi
vxm = 0
vym = 2*np.pi*24/30
vxj = 0
vyj = 2*np.pi*13/30

for i in range(N):
    xj+=h*vxj
    yj+= h*vyj
    x+=h*vx
    y+=h*vy
    xm+=h*vxm
    ym+=h*vym
    vxm += h*qddx(xm,ym, 4*np.pi*np.pi) + h*qddx(xm-x,ym-y, 4*np.pi*np.pi*1/333000)+ h*qddx(xm-xj,ym-yj, 4*np.pi*np.pi*1/1047)
    vym+=h*qddy(xm,ym, 4*np.pi*np.pi) + h*qddy(xm-x,ym-y, 4*np.pi*np.pi*1/333000)+ h*qddy(xm-xj,ym-yj, 4*np.pi*np.pi*1/1047)
    vxj += h*qddx(xj,yj, 4*np.pi*np.pi) + h*qddx(xj-x,yj-y, 4*np.pi*np.pi*1/333000) + h*qddx(xj-xm,yj-ym, 4*np.pi*np.pi*1/3112676)
    vyj+=h*qddy(xj,yj, 4*np.pi*np.pi) + h*qddy(xj-x,yj-y, 4*np.pi*np.pi*1/333000) + h*qddy(xj-xm,yj-ym, 4*np.pi*np.pi*1/3112676)
    vx+=h*qddx(x,y, 4*np.pi*np.pi) + h*qddx(x-xj,y-yj, 4*np.pi*np.pi*1/1047) + h*qddx(x-xm,y-ym, 4*np.pi*np.pi*1/3112676)
    vy+=h*qddy(x,y, 4*np.pi*np.pi) + h*qddy(x-xj,y-yj, 4*np.pi*np.pi*1/1047) + h*qddy(x-xm,y-ym, 4*np.pi*np.pi*1/3112676)
    qx[i] = x
    qy[i] = y
    qxj[i] = xj
    qyj[i] = yj
    qxm[i] = xm
    qym[i] = ym
    

#np.savetxt("neptunex.txt", qx)
#np.savetxt("neptuney.txt", qy)
#np.savetxt("neptunedx.txt", qxd)


#t = np.linspace(0, L, N+1)
#plt.plot(t, q1, "yellow", label = "neptune")
#plt.plot(t, q2, "red", label = "uranus")
plt.plot(qx, qy, "blue", label = "earth")
plt.plot(qxj, qyj, "green", label = "jupiter")
plt.plot(qxm, qym, "red", label = "mars")
plt.legend()
plt.show()


